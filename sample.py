#!/usr/bin/python3
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018, Baron.Valium <danshaku@mayoi.net>
#

from notifiers import Notifiers
import json

#### SETTINGS
# For Dicord
DISCORD_WEBHOOK_URL=''
DISCORD_USERNAME=''
# For Slack
SLACK_WEBHOOK_URL=''
SLACK_USERNAME=''
# For Line Notify
LINE_TOKEN=''
#####


## Create Notifyers instance
ns = Notifiers()

if (DISCORD_WEBHOOK_URL != ''):
    ns.add('Discord', url=DISCORD_WEBHOOK_URL, username=DISCORD_USERNAME)
if (SLACK_WEBHOOK_URL != ''):
    ns.add('Slack', url=SLACK_WEBHOOK_URL, username=SLACK_USERNAME)
if (LINE_TOKEN != ''):
    ns.add('Line', token=LINE_TOKEN)

## Send message
ns.send('Test Message for All notifier!!!')

