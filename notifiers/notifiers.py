#!/usr/bin/python3
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018, Baron.Valium <danshaku@mayoi.net>
#

from .notifier import *

class Notifiers:
    
    __notifiers = []

    # constructor
    def __init__(self, *args, **kwargs):
        pass

    # create based on class name:
    @staticmethod
    def factory(type, *args, **kwargs):
        return eval(type)(*args, **kwargs)

    #factory = staticmethod(factory)    

    # add notifyer object to list
    def add(self, type, *args, **kwargs):
        n_ = Notifiers.factory(type, *args, **kwargs)
        if (n_ != None):
            self.__notifiers.append(n_)
        return n_
        
    # send function
    def send(self, message):
        for n_ in self.__notifiers:
            n_.send(message)
    
if __name__ == '__main__':
    pass


