#!/usr/bin/python3
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018, Baron.Valium <danshaku@mayoi.net>
#

import requests
import json

class Notifier:
    
    # constructor
    def __init__(self, *args, **kwargs):
        pass
    
    # send function abstract method
    def send(self, message):
        pass

# Line class
class Line(Notifier):
    
    __token = ''
    __url = 'https://notify-api.line.me/api/notify'

    # constructor
    def __init__(self, *args, **kwargs):
        if 'token' in kwargs:
            self.__token = kwargs['token']

    # send function
    def send(self, message):
        payload_ = {'message': '\n' + message}
        headers_ = {'Authorization': 'Bearer ' + self.__token}
        requests.post(self.__url, data=payload_, headers=headers_)


# Discord class
class Discord(Notifier):
    
    __url = ''
    __username = 'python:notifier.discord'
    
    # constructor
    def __init__(self, *args, **kwargs):
        if 'url' in kwargs:
            self.__url = kwargs['url']
        if 'username' in kwargs:
            self.__username = kwargs['username']

    def send(self, message):
        requests.post(self.__url, data={'username': self.__username, 'content': message})


# Sleck class
class Slack(Notifier):
    
    __url = ''
    __username = 'python:notifyer.slack'
    
    # constructor
    def __init__(self, *args, **kwargs):
        if 'url' in kwargs:
            self.__url = kwargs['url']
        if 'username' in kwargs:
            self.__username = kwargs['username']

    def send(self, message):
        requests.post(self.__url, data=json.dumps({'username':self.__username, 'text':message}))

if __name__ == '__main__':
    pass